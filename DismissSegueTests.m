//
//  DismissSegueTests.m
//  P101XP
//
//  Created by Andrey Kiselev on 13.09.17.
//  Copyright © 2017 101xp. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>

#import "DismissSegue.h"

@interface DismissSegue (Tests)

- (void)perform;

@end

@interface DismissSegueTests : XCTestCase

@end

@implementation DismissSegueTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testPerform {
    DismissSegue *dismissSegue = [DismissSegue alloc];
    UIViewController *viewController = [[UIViewController alloc] init];
    id mockViewController = OCMPartialMock(viewController);
    id mockDismissSegue = OCMPartialMock(dismissSegue);
    OCMStub([mockDismissSegue sourceViewController]).andReturn(mockViewController);
    OCMStub([mockViewController presentingViewController]).andReturn(viewController);
    OCMStub([mockDismissSegue sourceViewController]).andReturn(mockViewController);
    OCMStub([mockViewController dismissViewControllerAnimated:YES completion:OCMOCK_ANY]).andDo(^(NSInvocation *invocation){
        void(^handlerBlock)();
        [invocation getArgument:&handlerBlock atIndex:3];
        handlerBlock();
    });
    OCMExpect([mockViewController presentViewController:OCMOCK_ANY
                                             animated:YES
                                           completion:nil]);
    
    [dismissSegue perform];
    
    OCMVerifyAll(mockViewController);
}

@end
